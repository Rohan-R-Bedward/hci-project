@extends('master')
@section('content')
<header id="fh5co-header" class="fh5co-cover fh5co-cover-sm" role="banner"
    style="background-image:url(images/Student-utech.jpg);" data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-7 text-left">
                <div class="display-t">
                    <div class="display-tc animate-box" data-animate-effect="fadeInUp">
                        <h1 class="mb30">Campus Experience</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<div id="fh5co-team">;
    <h1 class="reg_head" style="margin: 0 40px 0 40px; text-decoration: underline;">Accommodations</h1>
    <a name="Accommodations"></a>
    <div class="card mb-3 fh5co" style="margin:40px; max-width: 100%">
        <div class="row g-0">
            <div class="col-md-4">
                <img src="images/accommodation.jpg" alt="..." style="height:200px;" class="img-fluid" />
            </div>
            <div class="col-md-8">
                <div class="card-body">
                    <p class="card-header"><span style="margin-left:1em"> Mission Statement of the Division
                            of Student Services and Registry</p>
                    <p class="description">
                        <span style="margin-left:1em">
                            To provide quality service to facilitate learning and to engender goodwill towards the
                            University.<br>
                            <span style="margin-left:1em">The Accommodation Unit<br>
                                <span style="margin-left:1em">Student Services Department<br>
                                    <span style="margin-left:1em">University of Technology, Jamaica 237 Old Hope Road,
                                        Kingston 6<br>
                                        <span style="margin-left:1em">Telephone: (876) 927-1680-8<br>

                                            <br>
                    </p>
                    <p class="card-text">
                        <small class="text-muted">Last updated 3 mins ago</small>
                        <br>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <!-- Dining & Food Outlets -->
    <h1 class="reg_head" style="margin:80px 40px 40px 40px; text-decoration: underline;">Dining & Food Outlets</h1>
    <a name="Dining"></a>
    <div class="card mb-3 fh5co" style="margin:40px; max-width: 100%">
        <div class="row g-0">
            <div class="col-md-4">
                <img src="images/lillians.png" alt="..." style="height:150px;" class="img-fluid" />
            </div>
            <div class="col-md-8">
                <div class="card-body">
                    <p class="card-header">Lillian's Restaurant</p>
                    <p class="description">
                        Lillian's Restaurant is the official training restaurant <br>
                        of the School of Hospitality and Tourism Management <br>
                        It is located adjacent to the The restaurant <br>
                        Caribbean Sculpture Park at the University of Technology, Jamaica.<br>
                        provides dining and catering services to the UTech community and the
                        general public.<br>
                        Opening Hours:<br>
                        Monday to Friday - 8:01am to 6:00 pm<br>
                        Saturday to Sunday - Closed<br>
                        Contact#: (876) 927-2224<br>
                    </p>
                    <p class="card-text">
                        <small class="text-muted">Last updated 3 mins ago</small>
                    </p>
                </div>
            </div>
        </div>
    </div>



    <!-- Dining & Food Outlets -->
    <a name="Dining"></a>
    <div class="card mb-3 fh5co" style="margin:40px; max-width: 100%">
        <div class="row g-0">
            <div class="col-md-4">
                <img src="images/juici.png" alt="..." style="height:200px;" class="img-fluid" />
            </div>
            <div class="col-md-8">
                <div class="card-body">
                    <p class="card-header">Juici Patties</p>
                    <p class="description">
                        Opening Hours (May change during summer semesters):<br>
                        Monday to Friday - 10:00 am to 8:00 pm<br>
                        Saturday - 10:00 am to 6:00 pm<br>
                        Contact#: (876) 343-3921<br>
                    </p>
                    <p class="card-text">
                        <small class="text-muted">Last updated 3 mins ago</small>
                    </p>
                </div>
            </div>
        </div>
    </div>


    <div class="card mb-3 fh5co" style="margin:40px; max-width: 100%">
        <div class="row g-0">
            <div class="col-md-4">
                <img src="images/bk.png" alt="..." class="img-fluid" style="height:200px;" />
            </div>
            <div class="col-md-8">
                <div class="card-body">
                    <p class="card-header">Burger King</p>
                    <p class="description">
                        Burger King is flamed-broiled burgers, fries and soft drinks at a good
                        value<br>
                        served quickly and consistently by friendly people in clean
                        surroundings.<br><br>
                        Opening Hours (May change during summer semesters):<br>
                        Monday to Friday - 10:00 am to 8:00 pm<br>
                        Saturday - 10:00 am to 6:00 pm<br>
                        Location: Student's Activity Centre a.k.a. The Barn<br>
                        Contact#: (876) 343-3921<br>
                    </p>
                    <p class="card-text">
                        <small class="text-muted">Last updated 3 mins ago</small>
                    </p>
                </div>
            </div>
        </div>
    </div>



    <!-- Medical Centre -->
    <h1 class="reg_head" style="margin:80px 40px 40px 40px; text-decoration: underline;">Medical Centre</h1>
    <a name="Medical"></a>
    <div class="card mb-3 fh5co" style="margin:40px; max-width: 100%">
        <div class="row g-0">
            <div class="col-md-4">
                <img src="images/medical.jpeg" alt="..." class="img-fluid" style="height:200px;" />
            </div>
            <div class="col-md-8">
                <div class="card-body">
                    <p class="card-header">Medical Centre</p>
                    <p class="description">
                        The Medical Centre continues to provide quality health care to the
                        University Community. The team of health care professionals is
                        multidisciplinary and the following services are offered:<br><br>
                        * Counselling<br>
                        * Environmental Health<br>
                        * Family Planning<br>
                        * General Medicine<br>
                        * Medical Laboratory<br>
                        * Referrals<br>
                        * Medical Form<br>
                        * Online Booking<br><br>


                        OPENING HOURS:<br>
                        Monday 8:00 a.m. – 4:00 p.m.<br>
                        Tuesday 8:00 a.m. – 4:00 p.m<br>
                        Wednesday 10:00 a.m. –4:00 p.m<br>
                        Thursday 8:00 a.m. – 4:00 p.m<br>
                        Friday 8:00 a.m. – 4:00 p.m<br>
                        Saturday: Closed<br>
                    </p>
                    <p class="card-text">
                        <small class="text-muted">Last updated 3 mins ago</small>
                    </p>
                </div>
            </div>
        </div>
    </div>


    <!-- Student Union -->
    <h1 class="reg_head" style="margin:80px 40px 40px 40px; text-decoration: underline;">Student Union</h1>
    <a name="Student"></a>
    <div class="card mb-3 fh5co" style="margin:40px; max-width: 100%">
        <div class="row g-0">
            <div class="col-md-4">
                <img src="images/union.jpg" alt="..." class="img-fluid" style="height:200px;" />
            </div>
            <div class="col-md-8">
                <div class="card-body">
                    <p class="card-header">Student Union</p>

                    <a href="https://www.utechstudentsunion.com/" class="description">
                        Student Governance is vital for any institution. The Students' Union
                        Council<br>
                        is the representational body, which 
                        consists of both elected and appointed members.<br>
						Office is located beside the book store.
						read more...
                    </a><br>
					<p class="description">
                    OPENING HOURS:<br>
                    Monday - Friday 8:00 a.m. – 6:00 p.m.<br>
                    Sat-Sunday -Closed<br>
					CONTACT#:876.970.2222-3
					</p>
                    <p class="card-text">
                        <small class="text-muted">Last updated 3 mins ago</small>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <!-- Library and Book Store -->
    <h1 class="reg_head" style="margin:80px 40px 40px 40px; text-decoration: underline;">Library and Book Store</h1>
    <a name="Library"></a>
    <div class="card mb-3 fh5co" style="margin:40px; max-width: 100%">
        <div class="row g-0">
            <div class="col-md-4">
                <img src="images/library.jpg" alt="..." class="img-fluid" style="height:200px;" />
            </div>
            <div class="col-md-8">
                <div class="card-body">

                    <p class="card-header">CALVIN MCKAIN LIBRARY</p>
                    <p class="description">
                        The Library aims to provide accurate and up to date <br>
                        information in whatever format available <br>
                        in support of the programmes offered by the university. <br><br>

                        Services Include:<br>
                        - Loan & Reservation of Materials<br>
                        - Reference & Research<br>
                        - Internet Access<br>
                        - Current Awareness<br>
                        - Information Literacy<br>
                        - Inter-library loan<br>
                        - Photocopying<br>
                        - Printing & scanning <br>

                        Opening Hours:<br><br>
                        Mon to Fri - 8:30 a.m - 10:00 p.m. <br>
                        Sat - 12:30 p.m. to 8:00 p.m.<br>
                        Contact#:(876) 927-1680
                        </p>
                        <p class="card-text">
                            <small class="text-muted">Last updated 3 mins ago</small>
                        </p>

                </div>
            </div>
        </div>
    </div>
    <div class="card mb-3 fh5co" style="margin:40px; max-width: 100%">
        <div class="row g-0">
            <div class="col-md-4">
                <img src="images/book.jpeg" alt="..." style="height:200px;" class="img-fluid" />
            </div>
            <div class="col-md-8">
                <div class="card-body">
                    <p class="card-header">Book Store</p>
                    <p class="description">
                        The company started at the University of Technology<br>
                        Jamaica catering mainly to the campus community by <br>
                        providing textbooks, trade books, snacks, stationery and other supplies.<br>
                        Opening Hours:<br><br>
                        Mon to Thurs: 8:00 a.m. – 6:30 p.m.<br>
                        Fri: 8:00 a.m. – 5:00 p.m.<br>
                        CONTACT#: (876) 702-2459 / 2460<br>
                        Paymaster Services also available.<br>
                    </p>
                    <p class="card-text">
                        <small class="text-muted">Last updated 3 mins ago</small>
                    </p>
                </div>
            </div>
        </div>
    </div>



<!-- The Chapel -->
<h1 class="reg_head" style="margin:80px 40px 40px 40px; text-decoration: underline;">Chapel</h1>
<div class="card mb-3 fh5co" style="margin:40px; max-width: 100%">
    <div class="row g-0">
        <div class="col-md-4">
            <img src="images/chapel.jpg" alt="..." class="img-fluid" style="height:200px;" />
        </div>
        <div class="col-md-8">
            <div class="card-body">

                <p class="card-header">Chapel</p>
                <p class="description">
                    For Worhsip and Prayer<br>
                    Opening Hours:<br><br>
                    Mon to Fri - 12:00 a.m. <br>
                    to 1:00 p.m.<br>
                    Sat to Sunday - Closed <br>
                    </p>
                    <p class="card-text">
                        <small class="text-muted">Last updated 3 mins ago</small>
                    </p>

            </div>
        </div>
    </div>
	</div>



    <!-- CENTRE ARTS -->
    <h1 class="reg_head" style="margin:80px 40px 40px 40px; text-decoration: underline;">Centre Arts</h1>
    <a name="Library"></a>
    <div class="card mb-3 fh5co" style="margin:40px; max-width: 100%">
        <div class="row g-0">
            <div class="col-md-4">
                <img src="images/Centre-arts.jpg" alt="..." class="img-fluid" style="height:200px;" />
            </div>
            <div class="col-md-8">
                <div class="card-body">

                    <p class="card-header">Centre Arts</p>
                    <p class="description">
                        Centre for the Arts was established in 1998 at the University of Technology,
                        Jamaica (UTech, Jamaica)<br>
                        to link the arts with science and technology. Its focus, like its strategic
                        positioning in the “heart” of <br>
                        the University’s campus, is geared toward helping its clients find their
                        centre – a place where they arrive at a <br>
                        greater consciousness, confidence, competence, and courage to fulfill their
                        potential.<br>
                        Opening Hours:<br><br>
                        Mon to Fri - 8:30 a.m. <br>
                        to 10:00 p.m.<br>
                        Sat - 12:30 p.m. to 8:00 p.m.<br>
                        </p>
                        <p class="card-text">
                            <small class="text-muted">Last updated 3 mins ago</small>
                        </p>

                </div>
            </div>
        </div>
		</div>
        <!-- EDUCOM -->
        <h1 class="reg_head" style="margin:80px 40px 40px 40px; text-decoration: underline;">EDUCOM</h1>
        <a name="Library"></a>
        <div class="card mb-3 fh5co" style="margin:40px; max-width: 100%">
            <div class="row g-0">
                <div class="col-md-4">
                    <img src="images/EDUCOM.jpg" alt="..." class="img-fluid" style="height:200px;" />
                </div>
                <div class="col-md-8">
                    <div class="card-body">

                        <p class="card-header">EDUCOM</p>
                        <p class="description">
                            MISSION STATEMENT<br><br>
                            To improve the quality of life of its members and their
                            families,<br>
                            through the provision of personalized financial solutions and
                            advice. <br>
                            Opening Hours:<br>
                            Mon to Fri - 8:30 a.m. <br>
                            to 3:00 p.m.<br>
                            Sat to Sunday - Closed<br>
                            CONTACT#:(876) 977-4120
                            </p>
                            <p class="card-text">
                                <small class="text-muted">Last updated 3 mins ago</small>
                            </p>

                    </div>
                </div>
            </div>
        </div>
		</div>

        @endsection