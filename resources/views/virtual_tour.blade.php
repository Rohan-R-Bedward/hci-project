<!DOCTYPE HTML>
<html>

<head>
    <title>UTech</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="user-scalable=no, initial-scale=0.5, width=device-width, viewport-fit=cover" />
    <link rel="preload" href="script.js?v=1604723278249" as="script" />
    <link rel="preload" href="media/panorama_BC42729D_A0B4_09C8_41D2_1085940AF230_lq.jpeg" as="image" />
    <meta name="description" content="Virtual Tour" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('images/utech-logo.png') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Free HTML5 Website Template by gettemplates.co" />
    <meta name="keywords"
        content="free website templates, free html5, free template, free bootstrap, free website template, html5, css3, mobile first, responsive" />
    <meta name="author" content="gettemplates.co" />

    <!-- Facebook and Twitter integration -->
    <meta property="og:title" content="" />
    <meta property="og:image" content="" />
    <meta property="og:url" content="" />
    <meta property="og:site_name" content="" />
    <meta property="og:description" content="" />
    <meta name="twitter:title" content="" />
    <meta name="twitter:image" content="" />
    <meta name="twitter:url" content="" />
    <meta name="twitter:card" content="" />

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">

    <!-- Animate.css -->
    <link rel="stylesheet" href="{{ asset('css/animate.css')}}">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="{{ asset('css/icomoon.css')}}">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css')}}">
    <!-- Magnific Popup -->
    <link rel="stylesheet" href="{{ asset('css/magnific-popup.css')}}">
    <!-- Theme style  -->
    <link rel="stylesheet" href="{{ asset('css/style.css')}}">
    <!-- Modernizr JS -->
    <script src="{{ asset('js/modernizr-2.6.2.min.js')}}"></script>
    <!-- FOR IE9 below -->
    <!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
    <![endif]-->

    <script src="lib/tdvplayer.js"></script>
    <script src="lib/ThreeWebGL.js"></script>
    <script type="text/javascript">
        var player;
        var playersPlayingTmp = [];
        var isInitialized = false;

        function loadTour() {
            if (isInitialized)
                return;

            isInitialized = true;

            var beginFunc = function (event) {
                if (event.name == 'begin') {
                    var camera = event.data.source.get('camera');
                    if (camera && camera.get('initialSequence') && camera.get('initialSequence').get('movements')
                        .length > 0)
                        return;
                }

                if (event.sourceClassName == "MediaAudio")
                    return;

                player.unbind('preloadMediaShow', beginFunc, player, true);
                player.unbindOnObjectsOf('PanoramaPlayListItem', 'begin', beginFunc, player, true);
                player.unbind('stateChange', beginFunc, player, true);
                window.parent.postMessage("tourLoaded", '*');

                disposePreloader();

                onVirtualTourLoaded();
            };

            var settings = new TDV.PlayerSettings();

            settings.set(TDV.PlayerSettings.CONTAINER, document.getElementById('viewer'));
            settings.set(TDV.PlayerSettings.SCRIPT_URL, 'script.js?v=1605022595426');
            settings.set(TDV.PlayerSettings.FLASH_EXPRESS_INSTALLER_URL, 'lib/ExpressInstall.swf');
            settings.set(TDV.PlayerSettings.FLASH_AUDIO_PLAYER_URL, 'lib/AudioPlayer.swf');
            settings.set(TDV.PlayerSettings.FLASH_PANORAMA_PLAYER_URL, 'lib/PanoramaRenderer.swf');
            settings.set(TDV.PlayerSettings.THREE_JS_WEBGL_URL, 'lib/ThreeWebGL.js');
            settings.set(TDV.PlayerSettings.WEBVR_POLYFILL_URL, 'lib/WebVRPolyfill.js');
            settings.set(TDV.PlayerSettings.HLS_URL, 'lib/Hls.js');
            window.tdvplayer = player = TDV.PlayerAPI.create(settings);
            player.bind('preloadMediaShow', beginFunc, player, true);
            player.bind('stateChange', beginFunc, player, true);
            player.bindOnObjectsOf('PanoramaPlayListItem', 'begin', beginFunc, player, true);
            player.bindOnObject('rootPlayer', 'start', function (e) {
                var queryDict = {};
                location.search.substr(1).split("&").forEach(function (item) {
                    var k = item.split("=")[0],
                        v = decodeURIComponent(item.split("=")[1]);
                    queryDict[k] = v
                });
                if ("media-index" in queryDict) {
                    setMediaByIndex(parseInt(queryDict["media-index"]) - 1);
                } else if ("media-name" in queryDict) {
                    setMediaByName(queryDict["media-name"]);
                }

                player.getById('rootPlayer').bind('tourEnded', function () {
                    onVirtualTourEnded();
                }, player, true);
            }, player, false);

            /* Listen messages */
            window.addEventListener('message', function (e) {
                //Listen to messages for make actions to player in the format function:param1,param2
                var action = e.data;
                if (action == 'pauseTour' || action == 'resumeTour') {
                    this[action].apply(this);
                }
            });
        }

        function pauseTour() {
            var playLists = player.getByClassName('PlayList');
            for (var i = 0, count = playLists.length; i < count; i++) {
                var playList = playLists[i];
                var index = playList.get('selectedIndex');
                if (index != -1) {
                    var item = playList.get('items')[index];
                    var itemPlayer = item.get('player');
                    if (itemPlayer && itemPlayer.pause) {
                        playersPlayingTmp.push(itemPlayer);
                        itemPlayer.pause();
                    }
                }
            }

            player.getById('pauseGlobalAudios')();
        }

        function resumeTour() {
            while (playersPlayingTmp.length) {
                var viewer = playersPlayingTmp.pop();
                viewer.play();
            }

            player.getById('resumeGlobalAudios')();
        }

        function onVirtualTourLoaded() {

        }

        function onVirtualTourEnded() {

        }

        function setMediaByIndex(index) {
            if (window.tdvplayer !== undefined) {
                var rootPlayer = window.tdvplayer.getById('rootPlayer');
                rootPlayer.setMainMediaByIndex(index);
            }
        }

        function setMediaByName(name) {
            if (window.tdvplayer !== undefined) {
                var rootPlayer = window.tdvplayer.getById('rootPlayer');
                rootPlayer.setMainMediaByName(name);
            }
        }

        function showPreloader() {
            var preloadContainer = document.getElementById('preloadContainer');
            preloadContainer.style.opacity = 1;
        }

        function disposePreloader() {
            var transitionEnd = transitionEndEventName();
            var preloadContainer = document.getElementById('preloadContainer');



            var transitionEndName = transitionEndEventName();
            if (transitionEndName) {
                preloadContainer.addEventListener(transitionEnd, hide, false);

                preloadContainer.style.opacity = 0;
            } else {
                hide();
            }

            function hide() {
                preloadContainer.style.visibility = 'hidden';
                preloadContainer.style.display = 'none';
            }

            function transitionEndEventName() {
                var i,
                    undefined,
                    el = document.createElement('div'),
                    transitions = {
                        'transition': 'transitionend',
                        'OTransition': 'otransitionend',
                        'MozTransition': 'transitionend',
                        'WebkitTransition': 'webkitTransitionEnd'
                    };

                for (i in transitions) {
                    if (transitions.hasOwnProperty(i) && el.style[i] !== undefined) {
                        return transitions[i];
                    }
                }

                return undefined;
            }
        }

        function onBodyClick() {
            document.body.removeEventListener("click", onBodyClick);
            document.body.removeEventListener("touchend", onBodyClick);
            loadTour();
        }

        function onLoad() {
            if (isOVRWeb()) {
                showPreloader();
                loadTour();
                return;
            }

            showPreloader();
            loadTour()
        }

        function playVideo(video) {
            function isSafariDesktopV11orGreater() {
                return /^((?!chrome|android|crios|ipad|iphone).)*safari/i.test(navigator.userAgent) && parseFloat(
                    /Version\/([0-9]+\.[0-9]+)/i.exec(navigator.userAgent)[1]) >= 11;
            }

            function detectUserAction() {
                var onVideoClick = function (e) {
                    if (video.paused) {
                        video.play();
                    }
                    video.muted = false;
                    e.stopPropagation();
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    video.removeEventListener('click', onVideoClick);
                    video.removeEventListener('touchend', onVideoClick);
                };
                video.addEventListener("click", onVideoClick);
                video.addEventListener("touchend", onVideoClick);
            }

            if (isSafariDesktopV11orGreater()) {
                video.muted = true;
                video.play();
            } else {
                var canPlay = true;
                var promise = video.play();
                if (promise) {
                    promise.catch(function () {
                        video.muted = true;
                        video.play();
                        detectUserAction();
                    });
                } else {
                    canPlay = false;
                }

                if (!canPlay || video.muted) {
                    detectUserAction();
                }
            }
        }

        function isOVRWeb() {
            return window.location.hash.substring(1).split('&').indexOf('ovrweb') > -1;
        }

        function switchViewTour() {
                document.getElementById("tour_btn").style.backgroundColor = "green";
                document.getElementById("map_btn").style.backgroundColor = "red";
                document.getElementById("viewer").style.display = "block";
                document.getElementById("map").style.display = "none";
                document.getElementsByClassName("switchbtn")[0].style.padding = "50% 0% 0% 6%";
                document.getElementById("viewer").style.margin = "0 5% 5% 6%";
        }

        function switchViewMap() {
                document.getElementById("tour_btn").style.backgroundColor = "red";
                document.getElementById("map_btn").style.backgroundColor = "green";
                document.getElementById("viewer").style.display = "none";
                document.getElementById("map").style.display = "block";
                document.getElementsByClassName("switchbtn")[0].style.padding = "5% 0% 0% 6%";
                document.getElementById("viewer").style.margin = "0% 0% 0% 0%";
                
        }
    </script>
    <style type="text/css">
        html,
        body {
            width: 100%;
            height: 100%;
            margin: 0;
            padding: 0;
            padding: env(safe-area-inset-top) env(safe-area-inset-right) env(safe-area-inset-bottom) env(safe-area-inset-left);
        }
        .switchbtn{
            padding: 50% 0% 0% 6%;
        }
        #viewer {
            background-color: #FFFFFF;
            z-index: 1;
            position: absolute;
            width: 90%;
            height: 80%;
            margin: 0 5% 5% 6%;
        }

        #preloadContainer {
            z-index: 2;
            position: relative;
            width: 100%;
            height: 100%;
            transition: opacity 1s;
            -webkit-transition: opacity 1s;
            -moz-transition: opacity 1s;
            -o-transition: opacity 1s;
        }
    </style>
</head>


<body onload="onLoad()">
    <div class="fh5co-loader"></div>
    <div id="page">
        <nav class="fh5co-nav" role="navigation">
            <div class="container">
                <div class="row">
                    <div class="col-xs-2 text-left">
                        <div id="fh5co-logo">
                            <div style="Display:inline-block;"><img src="images/utech-logo.png" style="height:70px;">
                            </div><a style="padding-top:10px;" href="{{ route('home') }}">University of Technology,
                                <span style="padding-bottom:5px;"> Jamaica</span>
                                <hr class="decaration_line1">
                                <hr class="decaration_line2"></a>
                        </div>
                    </div>
                    <div class="col-xs-10 text-right menu-1">
                        <ul>
                            <li><a href="{{ route('home') }}">Home</a></li>
                            <li>
                                <a href="{{ route('campus_experience') }}">Campus Experience</a>
                            </li>
                            <li><a href="{{ route('virtual_tour') }}">Virtual Tour</a></li>
                            <li class="has-dropdown">
                                <a href="#">Useful Links</a>
                                <ul class="dropdown">
                                    <li><a href="{{ asset('https://evisionweb.utech.edu.jm/sipr/') }}">Student
                                            Portal</a></li>
                                    <li><a href="{{ asset('https://utechonline.utech.edu.jm/login/index.php') }}">Utech
                                            Moodle</a></li>
                                    <li><a href="{{ asset('https://ecommerce.utech.edu.jm/') }}">Utech Payment
                                            Portal</a></li>
                                    <li><a href="{{ asset('http://www.ujs.news/') }}">News</a></li>
                                </ul>
                            </li>
                            <li><a href="{{ route('contact') }}">Contact</a></li>
                        </ul>
                    </div>
                </div>

            </div>
        </nav>


        <header id="fh5co-header" class="fh5co-cover fh5co-cover-sm" role="banner"
            style="background-image:url(images/Student-utech.jpg);" data-stellar-background-ratio="0.5">
            <div class="overlay"></div>
    </div>
    </div>
    </header>

    <div class="row animate-box row-pb-md fadeInUp animated-fast" data-animate-effect="fadeInUp">
        <div class="row animate-box row-pb-md fadeInUp animated-fast" data-animate-effect="fadeInUp">
            <div class="col-md-8 col-md-offset-2 text-left fh5co-heading">
                <h2>Virtual Tour</h2>
                <p>welcome to the University of Technology, Jamaica, virtual tour.</p>
            </div>
        </div>

        <div id="preloadContainer" style="background-color:rgba(255,255,255,1)">
            <div style=" position: absolute; left: 0%; top: 30%; width: 100.00%; height: 10.00%">
                <div style="text-align:left; color:#000; ">
                    <DIV STYLE="text-align:center;"><SPAN
                            STYLE="letter-spacing:0vh;color:#777777;font-size:1.48vh;font-family:Arial, Helvetica, sans-serif;">Loading
                            virtual tour. Please wait...</SPAN></DIV>
                    <p STYLE="margin:0; line-height:1.11vh;"><BR
                            STYLE="letter-spacing:0vh;color:#000000;font-size:1.11vh;font-family:Arial, Helvetica, sans-serif;" />
                    </p>
                    <DIV STYLE="text-align:center;"><SPAN
                            STYLE="letter-spacing:0vh;color:#777777;font-size:1.48vh;font-family:Arial, Helvetica, sans-serif;">created
                            with the trial of 3DVista VT Pro</SPAN></DIV>
                    <p STYLE="margin:0; line-height:1.11vh;"><BR
                            STYLE="letter-spacing:0vh;color:#000000;font-size:1.11vh;font-family:Arial, Helvetica, sans-serif;" />
                    </p>
                    <DIV STYLE="text-align:center;"><SPAN
                            STYLE="letter-spacing:0vh;color:#000000;font-size:1.11vh;font-family:Arial, Helvetica, sans-serif;"><SPAN
                                STYLE="color:#0000ff;"><A HREF="http://www.3dvista.com" TARGET="_blank"
                                    STYLE="text-decoration:none; color:inherit;"><SPAN
                                        STYLE="color:#999999;font-size:1.3vh;"><U>www.3DVista.com</U></SPAN></A></SPAN></SPAN>
                    </DIV>
                </div>
            </div>
        </div>
        <div id="viewer"></div>
        <div id="map" style="display: none; width: 90%; margin: 0 5% 0 5%;"> 
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4722.926160766152!2d-76.7458733851147!3d18.01824628770347!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8edb3ed77252d65f%3A0xbe483695dda08fef!2sUniversity%20of%20Technology%2C%20Jamaica!5e1!3m2!1sen!2sjm!4v1605094040848!5m2!1sen!2sjm"  width="100%" height="800" frameborder="0" style=" border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
        </div>   
        
        <div class="btn-group switchbtn">
            <button class="btn" id="tour_btn" style="background-color: green; color: white; font-weight:4px;" onclick="switchViewTour()">Virtual Tour</button>
            <button class="btn" id="map_btn" style="background-color: red; color: white; font-weight:4px;" onclick="switchViewMap()">Map</button>
        </div>
        <div ></div>
        <footer id="fh5co-footer" role="contentinfo">
            <div class="container">
                <div class="row row-pb-md">
                    <div class="col-md-4 fh5co-widget ">
                        <h3>University of Technology, Jamaica</h3>
                        <p>Motto: 'You will Achieve, We will Assist'.</p>
                    </div>
                    <div class="col-md-5 col-sm-4 col-xs-6 col-md-push-1 ">
                        <h3>Contact Us</h3>
                        <p>3rd Floor Administrative Building</p>
                        <p>University of Technology, Jamaica</p>
                        <p>237 Hope Road</p>
                        <p>Kingston 6</p>
                        <p>Telephone Number: 876-970-5087/5097</p>
                        <p>Email: theutechacademy@utech.edu.jm</p>
                        <p>Fax: 876-977-0847</p>
                    </div>

                    <div class="col-md-2 col-sm-4 col-xs-6 col-md-push-1 ">
                        <ul class="fh5co-footer-links">
                            <h3>Links</h3>
                            <li class="active"><a href="{{ route('home') }}">Home</a></li>
                            <li><a href="{{ route('campus_experience') }}">Campus Experience</a></li>
                            <li><a href="{{ route('virtual_tour') }}">Virtual Tour</a></li>
                            <li><a href="{{ asset('https://evisionweb.utech.edu.jm/sipr/') }}">Student Portal</a></li>
                            <li><a href="{{ asset('https://utechonline.utech.edu.jm/login/index.php') }}">UTech Moodle</a></li>
                            <li><a href="{{ asset('https://ecommerce.utech.edu.jm/') }}">UTech Payment Portal</a></li>
                            <li><a href="{{ asset('http://www.ujs.news/') }}">News</a></li>
                            <li><a href="{{ route('contact') }}">Contact</a></li>
                        </ul>
                    </div>
                </div>
                <div class="row copyright">
                    <div class="col-md-12 text-center">
                        <p>
                            <ul class="fh5co-social-icons">
                                <li><a href="#"><i class="icon-twitter"></i></a></li>
                                <li><a href="#"><i class="icon-facebook"></i></a></li>
                                <li><a href="#"><i class="icon-linkedin"></i></a></li>
                            </ul>
                        </p>
                    </div>
                </div>

            </div>
        </footer>
    </div>
    </div>


    <!-- jQuery -->
    <script src="{{ asset('js/jquery.min.js')}}"></script>
    <!-- jQuery Easing -->
    <script src="{{ asset('js/jquery.easing.1.3.js')}}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset('js/bootstrap.min.js')}}"></script>
    <!-- Waypoints -->
    <script src="{{ asset('js/jquery.waypoints.min.js')}}"></script>
    <!-- countTo -->
    <script src="{{ asset('js/jquery.countTo.js')}}"></script>
    <!-- Magnific Popup -->
    <script src="{{ asset('js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{ asset('js/magnific-popup-options.js')}}"></script>
    <!-- Stellar -->
    <script src="{{ asset('js/jquery.stellar.min.js')}}"></script>
    <!-- Main -->
    <script src="{{ asset('js/main.js')}}"></script>
    
</body>

</html>