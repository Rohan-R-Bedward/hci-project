<!DOCTYPE HTML>
<!--
	Concept by gettemplates.co
	Twitter: http://twitter.com/gettemplateco
	URL: http://gettemplates.co
-->
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" type="image/x-icon" href="images/utech-logo.png">
    <title>UTech</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Free HTML5 Website Template by gettemplates.co" />
    <meta name="keywords"
        content="free website templates, free html5, free template, free bootstrap, free website template, html5, css3, mobile first, responsive" />
    <meta name="author" content="gettemplates.co" />

    <!-- Facebook and Twitter integration -->
    <meta property="og:title" content="" />
    <meta property="og:image" content="" />
    <meta property="og:url" content="" />
    <meta property="og:site_name" content="" />
    <meta property="og:description" content="" />
    <meta name="twitter:title" content="" />
    <meta name="twitter:image" content="" />
    <meta name="twitter:url" content="" />
    <meta name="twitter:card" content="" />

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">

    <!-- Animate.css -->
    <link rel="stylesheet" href="{{ asset('css/animate.css')}}">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="{{ asset('css/icomoon.css')}}">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css')}}">
    <!-- Magnific Popup -->
    <link rel="stylesheet" href="{{ asset('css/magnific-popup.css')}}">
    <!-- Theme style  -->
    <link rel="stylesheet" href="{{ asset('css/style.css')}}">
    <!-- Modernizr JS -->
    <script src="{{ asset('js/modernizr-2.6.2.min.js')}}"></script>
    <!-- FOR IE9 below -->
    <!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->
</head>

<body>
    <div class="fh5co-loader"></div>
    <div id="page">
        <nav class="fh5co-nav" role="navigation">
            <div class="container">
                <div class="row">
                    <div class="col-xs-2 text-left">
                        <div id="fh5co-logo">
                            <div style="Display:inline-block;"><img src="images/utech-logo.png" style="height:70px;">
                            </div><a style="padding-top:10px;" href="{{ route('home') }}">University of Technology,
                            <span style="padding-bottom:5px;"> Jamaica</span>
                                <hr class="decaration_line1">
                                <hr class="decaration_line2"></a>
                        </div>
                    </div>
                    <div class="col-xs-10 text-right menu-1">
                        <ul>
                            <li><a href="{{ route('home') }}">Home</a></li>
                            <li>
                                <a href="{{ route('campus_experience') }}">Campus Experience</a>
                            </li>
                            <li><a href="{{ route('virtual_tour') }}">Virtual Tour</a></li>
                            <li class="has-dropdown">
                                <a href="#">Useful Links</a>
                                <ul class="dropdown">
                                    <li><a href="{{ asset('https://evisionweb.utech.edu.jm/sipr/') }}">Student
                                            Portal</a></li>
                                    <li><a href="{{ asset('https://utechonline.utech.edu.jm/login/index.php') }}">Utech
                                            Moodle</a></li>
                                    <li><a href="{{ asset('https://ecommerce.utech.edu.jm/') }}">Utech Payment
                                            Portal</a></li>
                                    <li><a href="{{ asset('http://www.ujs.news/') }}">News</a></li>
                                </ul>
                            </li>
                            <li><a href="{{ route('contact') }}">Contact</a></li>
                        </ul>
                    </div>
                </div>

            </div>
        </nav>
        <!-- jQuery -->
        @yield('content')

        <footer id="fh5co-footer" role="contentinfo">
            <div class="container">
                <div class="row row-pb-md">
                    <div class="col-md-4 fh5co-widget ">
                        <h3>University of Technology, Jamaica</h3>
                        <p>Motto: 'You will Achieve, We will Assist'.</p>
                    </div>
                    <div class="col-md-5 col-sm-4 col-xs-6 col-md-push-1 ">
                        <h3>Contact Us</h3>
                        <p>3rd Floor Administrative Building</p>
                        <p>University of Technology, Jamaica</p>
                        <p>237 Hope Road</p>
                        <p>Kingston 6</p>
                        <p>Telephone Number: 876-970-5087/5097</p>
                        <p>Email: theutechacademy@utech.edu.jm</p>
                        <p>Fax: 876-977-0847</p>
                    </div>

                    <div class="col-md-2 col-sm-4 col-xs-6 col-md-push-1 ">
                        <ul class="fh5co-footer-links">
                            <h3>Links</h3>
                            <li class="active"><a href="{{ route('home') }}">Home</a></li>
                            <li><a href="{{ route('campus_experience') }}">Campus Experience</a></li>
                            <li><a href="{{ route('virtual_tour') }}">Virtual Tour</a></li>
                            <li><a href="{{ asset('https://evisionweb.utech.edu.jm/sipr/') }}">Student Portal</a></li>
                            <li><a href="{{ asset('https://utechonline.utech.edu.jm/login/index.php') }}">Utech
                                    Moodle</a></li>
                            <li><a href="{{ asset('https://ecommerce.utech.edu.jm/') }}">Utech Payment Portal</a></li>
                            <li><a href="{{ asset('http://www.ujs.news/') }}">News</a></li>
                            <li><a href="{{ route('contact') }}">Contact</a></li>
                        </ul>
                    </div>
                </div>

                <div class="row copyright">
                    <div class="col-md-12 text-center">
                        <p>
                            <ul class="fh5co-social-icons">
                                <li><a href="#"><i class="icon-twitter"></i></a></li>
                                <li><a href="#"><i class="icon-facebook"></i></a></li>
                                <li><a href="#"><i class="icon-linkedin"></i></a></li>
                            </ul>
                        </p>
                    </div>
                </div>

            </div>
        </footer>
    </div>
    <div class="gototop js-top">
        <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
    </div>

    <!-- jQuery -->
    <script src="{{ asset('js/jquery.min.js')}}"></script>
    <!-- jQuery Easing -->
    <script src="{{ asset('js/jquery.easing.1.3.js')}}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset('js/bootstrap.min.js')}}"></script>
    <!-- Waypoints -->
    <script src="{{ asset('js/jquery.waypoints.min.js')}}"></script>
    <!-- countTo -->
    <script src="{{ asset('js/jquery.countTo.js')}}"></script>
    <!-- Magnific Popup -->
    <script src="{{ asset('js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{ asset('js/magnific-popup-options.js')}}"></script>
    <!-- Stellar -->
    <script src="{{ asset('js/jquery.stellar.min.js')}}"></script>
    <!-- Main -->
    <script src="{{ asset('js/main.js')}}"></script>
</body>

</html>