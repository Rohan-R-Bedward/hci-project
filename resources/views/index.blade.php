@extends('master')
@section('content')
	<header id="fh5co-header" class="fh5co-cover" role="banner" style="background-image:url(images/utech-background.jpg);" data-stellar-background-ratio="0.5">
		<div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-7 text-left">
					<div class="display-t">
						<div class="display-tc animate-box" data-animate-effect="fadeInUp">
							<h1 class="mb30" style="font-size: 50px;">Welcome to The University of Techology, Jamaica.</h1><span style="font-size: 20px; color: #FFD200;">Start your application:</span>
							<p style="padding-top: 10px;">
								<a href="https://ecommerce.utech.edu.jm/Application/login.asp" target="_blank" class="btn btn-primary">Get Started</a>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>

	<div id="fh5co-services" class="fh5co-bg-section border-bottom">
		<div class="container">
			<div class="row row-pb-md">
				<div class="col-md-8 col-md-offset-2 text-left animate-box" data-animate-effect="fadeInUp">
					<div class="fh5co-heading">
						<h2>About UTech, Jamaica</h2>
						<p>The University of Technology, Jamaica (UTech,Ja.), formerly the College of Arts, Science and Technology is located within the Greater Kingston Metropolitan Region in the parish of St. Andrew and occupies approximately 18.2 hectares. It lies to the east of the Hope Botanical Gardens within the Papine-Liguanea commercial centres.The campus is served by several bus routes and is within walking distance of the Mona Campus of the regional University of the West Indies and the University Hospital.</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-6 ">
					<div class="feature-center animate-box" data-animate-effect="fadeInUp">
						<h3>Philosophy</h3>
						<p>The university is committed to the total education of the individual as a social being and seeks to develop the whole person in terms of personal well-being and social and intellectual competence. It promotes life-long learning, personal development and service to the community.</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 ">
					<div class="feature-center animate-box" data-animate-effect="fadeInUp">
						<h3>Mission</h3>
						<p>To positively impact Jamaica and the wider Caribbean through high quality learning opportunities, research and value added solutions to government, industry and communities.</p>
					</div>
				</div>
				<div class="clearfix visible-sm-block"></div>
				<div class="col-md-4 col-sm-6 ">
					<div class="feature-center animate-box" data-animate-effect="fadeInUp">
						<h3>Vision</h3>
						<p>In 2025, the University of Technology, Jamaica will:<br> We are the #1 University in the Caribbean for work-ready leaders, committed to transforming students and society through high quality teaching, research and value added services.</p>
					</div>
				</div>

				<div class="clearfix visible-md-block"></div>

				<div class="col-md-4 col-sm-6 ">
					<div class="feature-center animate-box" data-animate-effect="fadeInUp">
						<h3>Goals</h3>
						<p>The University’s mission is achieved through the fulfilment of the following goals:</p>
						<ul>
						<li><span>To pursue excellence through the Institution's programmes</span></li>
						<li><span>and the work of faculty, non-academic staff and students;</span></li>
						<li><span>To respond to national and regional human-resource needs;</span></li>
						<li><span>To maintain and enhance the role of technical/vocational education; to provide flexible delivery systems that are able to serve students who follow non-traditional routes to higher education;</span></li>
						<li><span>To ensure gender equity;</span></li>
						<li><span>To provide support to students by offering scholarships and providing a system of organized financial aid;</span></li>
						<li><span>To establish and pursue partnerships and collaborative arrangements with individual supporters, professional associations, educational institutions, governments and corporate entities at regional, national and international levels;</span></li>
						<li><span>To provide opportunities for communication and information transfer with other institutions of higher learning;</span></li>
						<li><span>To foster and encourage applied product-oriented research as an integral part of staff responsibility.</span></li>
						</ul>
					</div>
				</div>
				<div class="clearfix visible-sm-block"></div>
				<div class="col-md-4 col-sm-6 ">
					<div class="feature-center animate-box" data-animate-effect="fadeInUp">
						<h3>Core Values</h3>
						<p>The core values that will guide the institution have been explicitly stated and it is our intention that these values will infuse everything we do.</p>
						<p><Strong>RESPECT</strong><br /> We believe in the dignity and intrinsic worth of all people and endeavour to celebrate individuals by fostering an appreciation of and respect for each other’s difference.</p>
						<p><strong>INTEGRITY</strong><br /> As a university community we value ethical behaviour in all our endeavours, whether scholarly, cultural or intellectual and expect all conduct to be grounded in integrity, mutual respect and civility.</p>
						<p><strong>EXCELLENCE</strong><br /> We value excellence in our instructional, administrative and managerial pursuits, and are dedicated to the provision of academic courses of the highest quality in an environment that encourages excellence in research and scholarly activity employing the most effective tools, technologies and facilities for learning.</p>
						<p><strong>INNOVATION</strong><br /> Innovation is encouraged inside and outside the classroom. We foster intellectual inquiry, exploration and discovery that transcend traditional boundaries in an atmosphere that celebrates creativity.</p>
						<p><strong>TEAM SPIRIT</strong><br /> We pledge to work together in a spirit of cooperation to enrich the cultural environment. We will employ a decision-making style that stresses participation and consultation amongst administrators, faculty and students.</p>
						<p><strong>ACCOUNTABILITY</strong><br /> In the spirit of efficiency and effectiveness, we will embrace ownership of all our responsibilities and accept the principle that we are accountable for our actions.</p>
						<p><strong>SERVICE</strong><br /> We are committed to excellence and high quality service in all interactions with the immediate and wider community, demonstrating that we see service as being fundamental to our operations in all areas of university life.</p>
						</div>
				</div>
				<div class="clearfix visible-md-block"></div>
			</div>
		</div>
	</div>
	<div id="fh5co-counter">
		<div class="container">

			<div class="row animate-box" data-animate-effect="fadeInUp">
				<div class="col-md-8 col-md-offset-2 text-left fh5co-heading">
					<h2>History and Achievements</h2>
					<p>1958: The original name of UTech, Ja. was the Jamaica Institute of Technology ; just over 50 students were enrolled and only four programmes were offered.</p>
				</div>
			</div>

			<div class="row">
				
				<div class="col-md-3 col-sm-6 animate-box" data-animate-effect="fadeInLeft">
					<div class="feature-center">
						<span class="icon">
							<i class="ti-download"></i>
						</span>
						<span class="counter"><span class="js-counter" data-from="0" data-to="15" data-speed="1500" data-refresh-interval="50">1</span>M+</span>
						<span class="counter-label">Utech Alumni</span>

					</div>
				</div>
				<div class="col-md-3 col-sm-6 animate-box" data-animate-effect="fadeInLeft">
					<div class="feature-center">
							<span class="icon">
								<i class="ti-face-smile"></i>
							</span>
							<span class="counter"><span class="js-counter" data-from="0" data-to="120" data-speed="1500" data-refresh-interval="50">1</span>+</span>
							<span class="counter-label">Academic Rewards</span>
						</div>
					</div>
					<div class="col-md-3 col-sm-6 animate-box" data-animate-effect="fadeInLeft">
						<div class="feature-center">
							<span class="icon">
								<i class="ti-briefcase"></i>
							</span>
							<span class="counter"><span class=" js-counter" data-from="0" data-to="90" data-speed="1500" data-refresh-interval="50">1</span>+</span>
							<span class="counter-label">Registerd Students</span>
						</div>
					</div>
					<div class="col-md-3 col-sm-6 animate-box" data-animate-effect="fadeInLeft">
						<div class="feature-center">
							<span class="icon">
								<i class="ti-time"></i>
							</span>
							<span class="counter"><span class="js-counter" data-from="0" data-to="12" data-speed="1500" data-refresh-interval="50">1</span>K+</span>
							<span class="counter-label">Utech Applicants</span>

						</div>
					</div>
						
				</div>
			</div>
	</div>

	<div id="fh5co-project">
		<div class="container">
			<div class="row row-pb-md">
				<div class="col-md-8 col-md-offset-2 text-left fh5co-heading  animate-box">
					<h2>UTech Media - Recent News</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-6 fh5co-project animate-box" data-animate-effect="fadeIn">
					<a href="http://jamaica-gleaner.com/article/news/20201013/utech-students-finals-ibm-coding-competition"><img src="images/ibmf.jpeg" alt="Free HTML5 Website Template by gettemplates.co" class="img-responsive">
						<div class="fh5co-copy">
							<h3>UTech students in finals of IBM coding competition</h3>
							<p>Jamaica Gleaner - featured story</p>
						</div>
						<h3>Featured Story - click to read story</h3>
					</a>
				</div>
				<div class="col-md-4 col-sm-6 fh5co-project animate-box" data-animate-effect="fadeIn">
					<a href="http://www.ujs.news/2020/10/03/students-encouraged-to-be-interdisciplinary-and-flexible-in-the-covid-19-workplace/"><img src="images/sports.jpg" alt="Free HTML5 Website Template by gettemplates.co" class="img-responsive">
						<div class="fh5co-copy">
							<h3>Pelicans Trampled Knights in Sports Championship</h3>
							<p>UJS News - Sports</p>
						</div>
						<h3>Sports - click to read story</h3>
					</a>
				</div>
				<div class="col-md-4 col-sm-6 fh5co-project animate-box" data-animate-effect="fadeIn">
					<a href="http://www.ujs.news/2016/10/06/praise-worship-filled-the-sculpture-park/"><img src="images/entertainment.jpg" alt="Free HTML5 Website Template by gettemplates.co" class="img-responsive">
						<div class="fh5co-copy">
							<h3>Praise & Worship filled the Sculpture Park</h3>
							<p>UJS News - Entertainment</p>
						</div>
						<h3>Entertainment - click to read story</h3>
					</a>
				</div>
			</div>
		</div>	
	</div>
	<div id="fh5co-header" class="fh5co-cover" role="banner" style="background-image:url(images/students.jpg);" data-stellar-background-ratio="0.5">
		<div class="container">
			<div class="row">
				<div class="col-md-7 text-left">
					<div class="display-t">
						<div class="display-tc animate-box" data-animate-effect="fadeInUp">
							<h1 class="mb30" style="font-size: 50px;  color: #f0ad4e; font-weight: bold;">UTech Top Achievers 2020.</h1>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="fh5co-blog" class="fh5co-bg-section">
		<div class="container">
			<div class="row animate-box row-pb-md" data-animate-effect="fadeInUp">
				<div class="col-md-8 col-md-offset-2 text-left fh5co-heading">
					<h2>Useful Links</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-4 animate-box" data-animate-effect="fadeInUp">
					<div class="fh5co-post">
					<a href="https://utechonline.utech.edu.jm/login/index.php">
						<div class="fh5co-copy">
							<h3>UTech Moodle</h3>
							<p>Website Purpose:</p>
							<ul>
								<li><p>Access Module Notes</p></li>
								<li><p>View grades for Academic year</p></li>
								<li><p>Message Teacher and Classmates</p></li>
							</ul>
						</div>
					</a>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 animate-box" data-animate-effect="fadeInUp">
					<div class="fh5co-post">
						<a href="http://www.ujs.news/">
							<div class="fh5co-copy">
								<h3>The Journalism Society of UTech, Jamaica (UJS)</h3>
								<p>The Journalism Society of UTech, Jamaica (UJS) is a student-led organization that provides the University of Technology and the wider community with credible news.</p>
							</div>
						</a>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 animate-box" data-animate-effect="fadeInUp">
					<div class="fh5co-post">
						<a href="https://evisionweb.utech.edu.jm/sipr/">
							<div class="fh5co-copy">
								<h3>UTech Student Portal</h3>
								<p>Website Purpose:</p>
							<ul>
								<li><p>Check Academic Records</p></li>
								<li><p>Check Payment Records</p></li>
								<li><p>Select Modules</p></li>
							</ul>
							</div>
						</a>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 animate-box" data-animate-effect="fadeInUp">
					<div class="fh5co-post">
						<a href="https://ecommerce.utech.edu.jm/">
							<div class="fh5co-copy">
								<h3>Utech Payment Portal</h3>
								<p>Website Purpose:</p>
							<ul>
								<li><p>Tuition Payments</p></li>
								<li><p>Dorm Accommodation Payment</p></li>
							</ul>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	@endsection