<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Navigation;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::get('/', [Navigation::class, 'home'])->name('home');
Route::get('/campus_experience', [Navigation::class, 'campus_experience'])->name('campus_experience');
Route::get('/virtual_tour', [Navigation::class, 'virtual_tour'])->name('virtual_tour');
Route::get('/contact', [Navigation::class, 'contact'])->name('contact');
