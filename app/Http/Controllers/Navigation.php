<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Navigation extends Controller
{
    public function home(){
        return view('index');
    }
    public function campus_experience() {
        return view('campus_experience');
    }
    public function virtual_tour() {
        return view('virtual_tour');
    }
    public function academics() {
        return view('academics');
    }
    public function contact() {
        return view('contact');
    }
}
